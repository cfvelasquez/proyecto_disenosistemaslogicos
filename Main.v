`timescale 1ns / 1ps
module Main(CLOCK_50, VGA_HS, VGA_VS, VGA_R, VGA_G, VGA_B, KEY, SW, LEDs, speaker);
	input CLOCK_50;
	input [8:0] SW;
	input [2:0] KEY;
	output VGA_HS, VGA_VS;
	output [3:0] VGA_R, VGA_B, VGA_G;
	output reg [7:0] LEDs;
	output speaker;
	
	reg CLOCK_25, clk;
	
	//1
	reg [9:0] enemy1X, enemy1Y;
	reg enemy1V, bounce1;
	wire [3:0] enemy1;
	
	//2
	reg [9:0] enemy2X, enemy2Y;
	reg enemy2V, bounce2;
	wire [3:0] enemy2;
	
	//3
	reg [9:0] enemy3X, enemy3Y;
	reg enemy3V, bounce3;
	wire [3:0] enemy3;

	reg [9:0] bulletX, bulletY;
	reg fire, hit;
	wire [3:0] bullet;
	
	reg [31:0] counter;
	reg [3:0] counterScore;
	wire [9:0] x, y;
	
	reg [9:0] shipX;
	wire [3:0] ship;
	
//	reg [2:0] idx;

//CLOCKS
	always @(posedge CLOCK_50)
		CLOCK_25 <= ~CLOCK_25;
		
	always @(posedge CLOCK_50)
		if (counter == 50000) 
		begin
			counter <= 0;
			clk <= ~clk;
		end else
			counter <= counter + 1;

//THE SHIP
	always @(posedge clk)
		if(KEY[1] && |shipX)
			shipX <= shipX - 1;
		else if(KEY[0] && shipX < 615)
			shipX <= shipX + 1;
	assign ship = (x > shipX && x < shipX+25 && y > 450 && y < 460) ? 4'b1111:0;
	
//THE BULLET
	always @(posedge clk)
		if(KEY[2] && ~fire) 
		begin
			bulletX <= shipX+12;
			fire <= 1;
		end 
		else if(fire && bulletY>0)
			bulletY <= bulletY - 1;
		else 
		begin
			fire <= 0;
			bulletY <= 450;
		end
	always@(posedge clk)
		if(bulletY > enemy1Y && bulletY < enemy1Y+25 && bulletX > enemy1X && bulletX < enemy1X+25) begin
			hit <= 1;
			counterScore = counterScore + 1;
			case(counterScore)
				4'b0000: LEDs = 8'b00000000;
				4'b0001: LEDs = 8'b00000001;
				4'b0010: LEDs = 8'b00000011;
				4'b0011: LEDs = 8'b00000111;
				4'b0100: LEDs = 8'b00001111;
				4'b0101: LEDs = 8'b00011111;
				4'b0110: LEDs = 8'b00111111;
				4'b0111: LEDs = 8'b01111111;
				4'b1000: LEDs = 8'b11111111;
			endcase
		end
		else if(bulletY > enemy2Y && bulletY < enemy2Y+25 && bulletX > enemy2X && bulletX < enemy2X+25) begin
			hit <= 1;
			counterScore = counterScore + 1;
			case(counterScore)
				4'b0000: LEDs = 8'b00000000;
				4'b0001: LEDs = 8'b00000001;
				4'b0010: LEDs = 8'b00000011;
				4'b0011: LEDs = 8'b00000111;
				4'b0100: LEDs = 8'b00001111;
				4'b0101: LEDs = 8'b00011111;
				4'b0110: LEDs = 8'b00111111;
				4'b0111: LEDs = 8'b01111111;
				4'b1000: LEDs = 8'b11111111;
			endcase
		end
		else if(bulletY > enemy3Y && bulletY < enemy3Y+25 && bulletX > enemy3X && bulletX < enemy3X+25) begin
			hit <= 1;
			counterScore = counterScore + 1;
			case(counterScore)
				4'b0000: LEDs = 8'b00000000;
				4'b0001: LEDs = 8'b00000001;
				4'b0010: LEDs = 8'b00000011;
				4'b0011: LEDs = 8'b00000111;
				4'b0100: LEDs = 8'b00001111;
				4'b0101: LEDs = 8'b00011111;
				4'b0110: LEDs = 8'b00111111;
				4'b0111: LEDs = 8'b01111111;
				4'b1000: LEDs = 8'b11111111;
			endcase
		end
		else
			hit <= 0;
	assign bullet = (fire && y > bulletY && y < bulletY + 25 && x > bulletX && x < bulletX + 3)?4'b1111:0;
	
//ENEMY
	always @(posedge clk)
		enemy1X <= enemy1V ? enemy1X - 1:enemy1X + 1;
		
	always@(posedge clk)
		if(enemy1X == 50 && ~bounce1) 
		begin
			enemy1V <= 0;
			bounce1 <= 1;
		end 
		else if(enemy1X == 565 && ~bounce1) 
		begin 
			enemy1V <= 1;
			bounce1 <= 1;
		end 
		else 
			bounce1 <= 0;
	
	always @(posedge clk)
		if(hit) 
			enemy1Y <= 0;
		else if(bounce1) 
			enemy1Y <= enemy1Y + 10;
		else if(enemy1Y > 450)
			enemy1Y <= 0;
			
	assign enemy1 = (x > enemy1X && x < enemy1X + 25 && y > enemy1Y && y < enemy1Y + 25) ? 4'b1111:0;


//MORE ENEMY 
	always @(posedge clk)
		enemy2X <= enemy2V?enemy2X - 1:enemy2X + 1;
		
	always@(posedge clk)
		if(enemy2X == 100 && ~bounce2) 
		begin
			enemy2V <= 0;
			bounce2 <= 1;
		end 
		else if(enemy2X == 515 && ~bounce2) 
		begin 
			enemy2V <= 1;
			bounce2 <= 1;
		end 
		else 
			bounce2 <= 0;
	
	always @(posedge clk)
		if(hit) 
			enemy2Y <= 0;
		else if(bounce2) 
			enemy2Y <= enemy2Y + 10;
		else if(enemy2Y>450)
			enemy2Y <= 0;
			
	assign enemy2 = (x>enemy2X && x<enemy2X+25 && y>enemy2Y && y<enemy2Y+25)?4'b1111:0;
	
	//THREE ENEMY 
	always @(posedge clk)
		enemy3X <= enemy3V?enemy3X - 1:enemy3X + 1;
		
	always@(posedge clk)
		if(enemy3X == 150 && ~bounce3) 
		begin
			enemy3V <= 0;
			bounce3 <= 1;
		end 
		else if(enemy3X == 465 && ~bounce3) 
		begin 
			enemy3V <= 1;
			bounce3 <= 1;
		end 
		else 
			bounce3 <= 0;
	
	always @(posedge clk)
		if(hit) 
			enemy3Y <= 0;
		else if(bounce3) 
			enemy3Y <= enemy3Y + 10;
		else if(enemy3Y>450)
			enemy3Y <= 0;
			
	assign enemy3 = (x > enemy3X && x < enemy3X + 25 && y > enemy3Y && y < enemy3Y + 25) ? 4'b1111:0;

/*	
	//ENEMY SHIPS
	always @ (x, y)
	begin
		if((x > 0 + enemy1X) && (x < 40 + enemy1X) && (y > 0 + enemy1Y) && (y < 40 + enemy1Y)) 
			idx <= 3'd7;
		else 
			idx <= 3'd0;
	end
*/
	
//VGA
	VGAsync vgas (.iClock(CLOCK_25), .oVGA_HS(VGA_HS), .oVGA_VS(VGA_VS), .oActive(Active), .oX(x), .oY(y));

//AUDIO
	BG_Music bgm (.clk(CLOCK_25), .speaker(speaker));
	
	assign VGA_R = (Active)?{SW[8:6],1'b0}|ship|bullet|enemy1|enemy2|enemy3:0;
	assign VGA_G = (Active)?{SW[5:3],1'b0}|ship|bullet|enemy1|enemy2|enemy3:0;
	assign VGA_B = (Active)?{SW[2:0],1'b0}|ship|bullet|enemy1|enemy2|enemy3:0;
	
endmodule
